Pinto Block 🐴

Set up bundle classes for the custom blocks for used by Layout Builder.

# Usage

## Bundle class setup

Bundle class:

```php
final class BlockBundle extends \Drupal\block_content\Entity\BlockContent {
  public function getSomething(): ?string {
    return $this->something->value ?? NULL;
  }
}
```

Use a `hook_entity_bundle_info_alter` hook, or implement via [BCA][bca] or
[Hux][hux].

**Traditional**

```php
/**
 * Implements hook_entity_bundle_info_alter().
 */
function mymodule_entity_bundle_info_alter(array &$bundles): void {
  $bundles['block_content']['foobar']['class'] = Foobar::class;
}
```

**BCA**

```php
#[\Drupal\bca\Attribute\Bundle(entityType: 'block_content', bundle: 'foobar', label: new TranslatableMarkup('Foobar'))]
final class BlockBundle extends BlockContent {}
```

**Hux**

```php
class Hooks {
  #[\Drupal\hux\Attribute\Alter('entity_bundle_info')]
  public function bundleAlter(array &$bundles): void {
    $bundles['block_content']['foobar']['class'] = BlockBundle::class;
  }
}
```

## Create a Pinto Theme Object

Create a Pinto Theme Object which implements the
`\Drupal\pinto_block\BlockBundleObjectInterface` interface. The structure of object, including the constructor, is up to you.

```php
use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\pinto\Object\DrupalObjectTrait;
use Drupal\pinto_block\BlockBundleObjectInterface;
use Drupal\pinto_block_test\Entity\BlockContent\BlockBundle;
use Pinto\Attribute\ThemeDefinition;

/**
 * Test object.
 */
#[ThemeDefinition(definition: [
  'variables' => [
    'test' => NULL,
  ],
])]
final class FoobarObject implements BlockBundleObjectInterface {

  use DrupalObjectTrait;

  private function __construct(
    private readonly BlockBundle $blockContent,
    private readonly ContentEntityInterface $entity,
    private readonly string $viewMode,
  ) {
  }

  /**
   * @phpstan-param \Drupal\pinto_block_test\Entity\BlockContent\BlockBundle $blockContent
   */
  public static function create(
    BlockContentInterface $blockContent,
    ContentEntityInterface $entity,
    string $viewMode,
  ): static {
    return new static($blockContent, $entity, $viewMode);
  }

  public function __invoke(): mixed {
    return $this->pintoBuild(function (mixed $build): mixed {
      return $build + [
        '#test' => [
          '#plain_text' => $this->blockContent->getSomething() ?? '',
        ],
      ];
    });
  }

}
```

## Associate theme object with the bundle class

Add a `\Drupal\pinto_block\Attribute\PintoBlock` attribute to the bundle class.

The `$objectClassName` argument is a class-string for the theme object.

```php
#[\Drupal\pinto_block\Attribute\PintoBlock(objectClassName: FoobarObject::class)]
final class Foobar extends BlockContent {}
```

 [hux]: https://www.drupal.org/project/hux
 [bca]: https://www.drupal.org/project/bca
