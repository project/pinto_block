<?php

declare(strict_types=1);

namespace Drupal\pinto_block;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Entity\ContentEntityInterface;

interface BlockBundleObjectInterface {

  /**
   * Creates an object for a block content.
   *
   * @param \Drupal\block_content\BlockContentInterface $blockContent
   *   The object class may not be the same as the configured bundle class if it
   *   was created and serialized before the bundle class was created and
   *   configured.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity this block is embedded onto.
   * @param string $viewMode
   *   The view mode.
   */
  public static function create(
    BlockContentInterface $blockContent,
    ContentEntityInterface $entity,
    string $viewMode,
  ): static;

}
