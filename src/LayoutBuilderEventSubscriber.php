<?php

declare(strict_types=1);

namespace Drupal\pinto_block;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\Plugin\Block\InlineBlock;
use Drupal\pinto_block\Attribute\PintoBlock;
use Pinto\PintoMapping;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class LayoutBuilderEventSubscriber implements EventSubscriberInterface {

  /**
   * Cache of block content bundle class to Pinto Theme Object class.
   *
   * @var array<class-string<\Drupal\block_content\BlockContentInterface>, class-string<\Drupal\pinto_block\BlockBundleObjectInterface>|false>
   */
  private $objectClassNameMapping = [];

  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private PintoMapping $mapping,
  ) {
  }

  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event): void {
    if (!$event->getPlugin() instanceof InlineBlock) {
      return;
    }

    $build = $event->getBuild();

    /** @var \Drupal\block_content\BlockContentInterface $blockContent */
    $blockContent = $build['content']['#block_content'] ?? throw new \LogicException('Missing block content entity');

    if (FALSE === $blockContent->isNew()) {
      // A saved (not isNew) block content will be one restored from storage
      // during this request, and therefore should already have an accurate
      // bundle class.
      $bundleClass = $blockContent::class;
    }
    else {
      $blockContentBundle = $build['#derivative_plugin_id'] ?? throw new \LogicException('Missing #derivative_plugin_id');

      // We cannot rely on the class of the $build['#block_content'] since the
      // serialized entity may not be an instance of the bundle class (e.g if
      // unsaved bundle-level or work-in-progress entity level.
      /** @var class-string<\Drupal\block_content\BlockContentInterface> $bundleClass */
      $bundleClass = $this->blockContentStorage()->getEntityClass($blockContentBundle);
    }

    /** @var class-string<\Drupal\pinto_block\BlockBundleObjectInterface>|FALSE $objectClassName */
    $objectClassName = $this->objectClassNameMapping[$bundleClass] ??= (((new \ReflectionClass($bundleClass))->getAttributes(PintoBlock::class)[0] ?? NULL)?->newInstance()->objectClassName) ?? FALSE;
    if ($objectClassName === FALSE) {
      return;
    }

    /** @var \Drupal\Core\Plugin\Context\EntityContext|null $contextEntity */
    $contextEntity = $event->getContexts()['layout_builder.entity'] ?? NULL;
    if ($contextEntity === NULL) {
      return;
    }

    $entity = $contextEntity->getContextValue();
    if (!$entity instanceof ContentEntityInterface) {
      throw new \Exception('Expected an entity context value');
    }

    // Replace in its entirety.
    // @todo maybe selectively only non '#'-prefixed?
    // An object is added in the render array, this will be appropriately
    // rendered by \Drupal\pinto\Renderer.
    $object = $objectClassName::create(
      $blockContent,
      $entity,
      $build['content']['#view_mode'] ?? throw new \LogicException('Missing view mode'),
    );

    $methodName = $this->mapping->getBuildInvoker($object::class);
    // @phpstan-ignore-next-line
    $build['content'] = \call_user_func([$object, $methodName]);

    $event->setBuild($build);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = ['onBuildRender', 100];
    return $events;
  }

  private function blockContentStorage(): ContentEntityStorageInterface {
    return $this->entityTypeManager->getStorage('block_content');
  }

}
