<?php

declare(strict_types=1);

namespace Drupal\pinto_block\Attribute;

/**
 * An attribute for representing which class an enum represents.
 */
#[\Attribute(flags: \Attribute::TARGET_CLASS)]
final class PintoBlock {

  /**
   * Constructs a pinto block attribute.
   *
   * @phpstan-param class-string<\Drupal\pinto_block\BlockBundleObjectInterface> $objectClassName
   */
  public function __construct(
    public string $objectClassName,
  ) {
  }

}
