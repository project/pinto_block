<?php

declare(strict_types=1);

namespace Drupal\pinto_block_test\Entity\BlockContent;

use Drupal\block_content\Entity\BlockContent;
use Drupal\pinto_block\Attribute\PintoBlock;
use Drupal\pinto_block_test\Pinto\BlockBundleObject;

#[PintoBlock(BlockBundleObject::class)]
final class BlockBundle extends BlockContent {

  public const BUNDLE = 'block_bundle';

  public function getBody(): ?string {
    // @phpstan-ignore-next-line
    return $this->body->value ?? NULL;
  }

}
