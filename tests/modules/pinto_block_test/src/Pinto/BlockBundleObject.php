<?php

declare(strict_types=1);

namespace Drupal\pinto_block_test\Pinto;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\pinto\Object\DrupalObjectTrait;
use Drupal\pinto_block\BlockBundleObjectInterface;
use Drupal\pinto_block_test\Entity\BlockContent\BlockBundle;
use Pinto\Attribute\Asset\Css;
use Pinto\Attribute\Asset\Js;
use Pinto\Attribute\ThemeDefinition;

/**
 * Test object.
 */
#[Css('styles.css')]
#[Js('app.js')]
final class BlockBundleObject implements BlockBundleObjectInterface {

  use DrupalObjectTrait;

  /**
   * Constructor.
   */
  private function __construct(
    private readonly BlockBundle $blockContent,
    private readonly ContentEntityInterface $entity,
    private readonly string $viewMode,
  ) {
  }

  /**
   * Creates a new object.
   */
  public static function create(
    BlockContentInterface $blockContent,
    ContentEntityInterface $entity,
    string $viewMode,
  ): static {
    \assert($blockContent instanceof BlockBundle);
    return new static($blockContent, $entity, $viewMode);
  }

  public function __invoke(): mixed {
    return $this->pintoBuild(function (mixed $build): mixed {
      return $build + [
        '#test_text' => [
          '#markup' => $this->blockContent->getBody() . ' rendered on ' . $this->entity->label() . ' in view mode ' . $this->viewMode,
        ],
      ];
    });
  }

  #[ThemeDefinition]
  public static function theme(): array {
    return [
      'variables' => [
        'test_text' => NULL,
      ],
    ];
  }

}
