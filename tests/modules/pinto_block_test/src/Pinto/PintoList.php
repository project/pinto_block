<?php

declare(strict_types=1);

namespace Drupal\pinto_block_test\Pinto;

use Pinto\Attribute\Definition;
use Pinto\List\ObjectListInterface;
use Pinto\List\ObjectListTrait;
use function Safe\realpath;

enum PintoList: string implements ObjectListInterface {
  use ObjectListTrait;

  #[Definition(BlockBundleObject::class)]
  case Pinto_Object = 'block_bundle_object';

  public function templateDirectory(): string {
    return '@pinto_block_test/templates';
  }

  public function cssDirectory(): string {
    return \substr(realpath(__DIR__ . '/../../resources/'), \strlen(\DRUPAL_ROOT));
  }

  public function jsDirectory(): string {
    return \substr(realpath(__DIR__ . '/../../resources/'), \strlen(\DRUPAL_ROOT));
  }

}
